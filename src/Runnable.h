/*
 * Runnable.h
 *
 *  Created on: 21.11.2015
 *      Author: markusfasel
 */

#ifndef RUNNABLE_H_
#define RUNNABLE_H_

#include <pthread.h>

class Runnable {
public:
	Runnable();
	virtual ~Runnable() {}

	bool StartThread();
	void JoinThread();

protected:
	virtual void Run() = 0;

private:
	static void *ExecuteThread(void *content);
	pthread_t				fthread;
};

#endif /* RUNNABLE_H_ */
