/*
 * ZMQconnector.h
 *
 *  Created on: 22.11.2015
 *      Author: markusfasel
 */

#ifndef ZMQCONNECTOR_H_
#define ZMQCONNECTOR_H_

#include "Runnable.h"

#include <string>

class Merger;
class AliHLTDataTopic;

class ZMQconnector : public Runnable {
public:
	ZMQconnector();
	virtual ~ZMQconnector();

	void InitConnection();

	int GetConnectionStatus() const { return fZMQConnectionStatus; }

	void SetMerger(Merger *merger) { fMerger = merger; }
	void SetStopSignal(bool doStop) { fStopSignal = doStop; }
	void SetPollingTimeout(int pollingTimeout) { fZMQPollingTimeout = pollingTimeout * 1000; } // In seconds
	void SetPollingInterval(int pollingInterval) { fZMQPollingInterval = pollingInterval * 1000000; } // In seconds
	void SetConnectionString(std::string connectionString) { fZMQConnectionString = connectionString; }

	void SetLock() { fLock = true; }
	void ReleaseLock() { fLock = false; }

	void SetVerbose(bool verbose) { fVerbose = verbose; }

protected:
	virtual void Run();

	bool IsStartOfRun(AliHLTDataTopic &) const;
	bool IsEndOfRun(AliHLTDataTopic &) const;
	int GetRunNumber() const;

private:
	Merger					*fMerger;

	std::string 			fZMQConnectionString;
	int						fZMQConnectionMode;
	int						fZMQConnectionStatus;
	int						fZMQPollingTimeout;				// In milliseconds
	int 					fZMQPollingInterval;			// In microseconds
	void 					*fZMQcontext;
	void 					*fZMQsocket;

	bool					fConnectionInitialized;
	bool					fStopSignal;
	bool					fLock;

	bool 					fVerbose;
};

#endif /* ZMQCONNECTOR_H_ */
