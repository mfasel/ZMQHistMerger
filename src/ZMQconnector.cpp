/*
 * ZMQconnector.cpp
 *
 *  Created on: 22.11.2015
 *      Author: markusfasel
 */

#include <cstring>
#include <unistd.h>

#include "zmq.h"

#include <TObjArray.h>
#include <TString.h>

#include "AliHLTDataTypes.h"
#include "AliZMQhelpers.h"

#include "Merger.h"
#include "ZMQconnector.h"

ZMQconnector::ZMQconnector() :
Runnable(),
fMerger(NULL),
fZMQConnectionString(""),
fZMQConnectionMode(-1),
fZMQConnectionStatus(0),
fZMQPollingTimeout(1000), 			// 1s
fZMQPollingInterval(0),
fZMQcontext(NULL),
fZMQsocket(NULL),
fConnectionInitialized(kFALSE),
fStopSignal(kFALSE),
fLock(false),
fVerbose(false)
{
}

ZMQconnector::~ZMQconnector() {
	zmq_close(fZMQsocket);
	zmq_ctx_destroy(fZMQcontext);
}

void ZMQconnector::Run(){
	if(!fConnectionInitialized) InitConnection();
	while(1){
		if(fStopSignal) break;

		if(fZMQConnectionMode == ZMQ_REQ) {
			//if (fVerbose) Printf("sending request");
			zmq_send(fZMQsocket, "*", 1, ZMQ_SNDMORE);
			zmq_send(fZMQsocket, "", 4, 0);
		}

		//wait for the data
		zmq_pollitem_t sockets[] = {
				{ fZMQsocket, 0, ZMQ_POLLIN, 0 },
		};
		zmq_poll(sockets, 1, (fZMQConnectionMode==ZMQ_REQ) ? fZMQPollingTimeout : -1);

		if (!sockets[0].revents & ZMQ_POLLIN) {
			//server died
			Printf("connection timed out");
			fZMQConnectionMode = alizmq_socket_init(fZMQsocket, fZMQcontext, fZMQConnectionString);
			if (fZMQConnectionMode < 0){
				fZMQConnectionStatus = 1;
				return;
			}
			continue;
		} else {
			//get all data (topic+body), possibly many of them
			aliZMQmsg message;
			alizmq_msg_recv(&message, fZMQsocket, 0);
			if(fLock){
				// wait until thread is unlocked
				while(fLock);
			}
			TObjArray *histBuffer(NULL);
			for (aliZMQmsg::iterator i=message.begin(); i!=message.end(); ++i) {
				TObject* object;
				AliHLTDataTopic topic;
				alizmq_msg_iter_topic(i, topic);
				if(IsStartOfRun(topic)){
					alizmq_msg_iter_data(i, object);
					AliHLTRunDesc *rundesc = dynamic_cast<AliHLTRunDesc *>(object);
					fMerger->SetRunNumber(rundesc->fRunNo);
					fMerger->SetStartOfRun();
					if(fVerbose) Printf("ZMQ Connector: SOR signal received - run %d\n", rundesc->fRunNo);
				} else if(IsEndOfRun(topic)) {
					if(fVerbose) Printf("ZMQ Connector: EOR signal received\n");
					fMerger->SetEndOfRun();
				} else {
					if(!fMerger->IsRunStatusActive()){
						// missed SOR message - set status to active anyhow
						// check whether we can still get the run number
						if(fVerbose) Printf("ZMQ Connector: SOR signal missing - setting arbitrary run number\n");
						fMerger->SetStartOfRun();
						fMerger->SetRunNumber(100); 	// Arbitrary - not known any more
					}
					alizmq_msg_iter_data(i, object);
					if (object){
						if(!histBuffer){
							histBuffer = new TObjArray;
							histBuffer->SetOwner(true);
						}
						// check whether object is an EMCAL histogram
						if(fVerbose) Printf("ZMQ connector - object %s received\n", object->GetName());
						if(!TString(object->GetName()).Contains("EMC")) continue;
						histBuffer->Add(object);
					}
				}
			}
			alizmq_msg_close(&message);

			// forward data to the merger
			fMerger->SetPayload(histBuffer);
		}//socket 0
		// Set process to sleep
		usleep(fZMQPollingInterval);
	}
}

bool ZMQconnector::IsStartOfRun(AliHLTDataTopic &topic) const {
	return !memcmp(topic.GetID().c_str(), kAliHLTDataTypeSOR.fID, kAliHLTComponentDataTypefIDsize);
}

bool ZMQconnector::IsEndOfRun(AliHLTDataTopic &topic) const {
	return !memcmp(topic.GetID().c_str(), kAliHLTDataTypeEOR.fID, kAliHLTComponentDataTypefIDsize);
}

void ZMQconnector::InitConnection(){
	fZMQcontext = zmq_ctx_new();
	fZMQConnectionMode = alizmq_socket_init(fZMQsocket, fZMQcontext, fZMQConnectionString);
	if(fZMQConnectionMode >= 0) fConnectionInitialized = kTRUE;
}
