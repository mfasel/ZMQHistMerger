/*
 * Merger.h
 *
 *  Created on: 21.11.2015
 *      Author: markusfasel
 */

#ifndef MERGER_H_
#define MERGER_H_

#include "Runnable.h"
#include <ctime>

class TH1;
class TObjArray;
class ZMQconnector;

class Merger : public Runnable{
public:
	Merger();
	virtual ~Merger();

	void SetStopSignal(bool stop) { fStopSignal = stop; }
	void SetPayload(TObjArray *payload);

	void SetRunNumber(int runnumber) { fRunNumber = runnumber; }
	void SetMergingInterval(int mergeInterval) { fMergingInterval = mergeInterval; }

	bool IsRunStatusActive() const { return fRunStatus; }

	void SetStartOfRun() { fRunStatus = true; }
	void SetEndOfRun() { fRunStatus = false; }

	void SetZMQConnector(ZMQconnector *connector) { fZMQconnector = connector; }

	void SetVerbose(bool verbose) { fVerbose = verbose; }

protected:
	virtual void Run();
	void ProcessPayloads(const struct tm &timestamp);
	TH1 *MergeBunch(const std::vector<TH1 *> &bunch);
	TH1 *CopyHist(const TH1 *input);

	ZMQconnector					*fZMQconnector;		// for locking

	bool							fStopSignal;		// Terminate thread

	TObjArray 						*fPayloads;			// Payload, set from outside
	TObjArray						*fWorking;			// Payload, currently in operation

	int								fMergingInterval;	//

	int								fRunNumber;			// Run number
	bool							fRunStatus;			// Run status

	bool							fInternalLock;		// Locking, set when internal process accesses common memory
	bool							fExternalLock;		// Locking, set when external process accesses common memory

	bool							fVerbose;			// Switch for verbosity
};

#endif /* MERGER_H_ */
