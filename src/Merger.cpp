/*
 * Merger.cpp
 *
 *  Created on: 21.11.2015
 *      Author: markusfasel
 */

#include <ctime>
#include <map>
#include <string>
#include <vector>

#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TObjArray.h"
#include "TProfile.h"

#include "ZMQconnector.h"
#include "Merger.h"

Merger::Merger() :
	Runnable(),
	fZMQconnector(NULL),
	fStopSignal(false),
	fPayloads(NULL),
	fWorking(NULL),
	fMergingInterval(60),
	fRunNumber(0),
	fRunStatus(false),
	fInternalLock(false),
	fExternalLock(false),
	fVerbose(false)
{
	fPayloads = new TObjArray;
	fWorking = new TObjArray;
	fWorking->SetOwner(true);
}

Merger::~Merger() {
	if(fPayloads) delete fPayloads;
}

void Merger::Run(){
	bool doRun = true, doMerge = false, runActive = fRunStatus;
	time_t last, now;
	time(&last);
	while(doRun){
		if(fStopSignal) doRun = false;				// terminate thread on request
		if(!runActive && fRunStatus){
			// Start of run
			// reset time of last merge
			runActive = true;
			time(&last);
		}
		// Check if we have new payloads to process
		if(fExternalLock) continue;

		// check whether exceeded the time limit or we ended the run
		if(runActive && !fRunStatus){
			doMerge = true;
			runActive = false;
		} else {
			// check whether we are beyond the merge iterval
			time(&now);
			if(difftime(now, last) > fMergingInterval) doMerge = true;
		}
		if(fPayloads->GetEntries()){
			fInternalLock = true;
			// Move everything to the working area so that we can accept new payloads
			for(TIter payloadIter = TIter(fPayloads).Begin(); payloadIter != TIter::End(); ++payloadIter){
				fWorking->Add(*payloadIter);
			}
			fPayloads->Clear();
			fInternalLock = false;
			ProcessPayloads(*localtime(&now));
			last = now;
		}
	}
}

void Merger::ProcessPayloads(const struct tm &timestamp){
	std::map<std::string, std::vector<TH1 *> > histos;
	// Assing histograms to the maps
	TH1 *histoprocess(NULL);
	for(TIter payloadIter = TIter(fWorking).Begin(); payloadIter != TIter::End(); ++payloadIter){
		TObjArray *mypayload = static_cast<TObjArray *>(*payloadIter);
		for(TIter contentIter = TIter(mypayload).Begin(); contentIter != TIter::End(); ++contentIter){
			histoprocess = dynamic_cast<TH1 *>(*contentIter);
			std::map<std::string, std::vector<TH1 *> >::iterator found = histos.find(histoprocess->GetName());
			if(found != histos.end())
				found->second.push_back(histoprocess);
			else {
				std::vector<TH1 *> newhistos;
				newhistos.push_back(histoprocess);
				histos.insert(std::pair<std::string, std::vector<TH1 *> >(histoprocess->GetName(), newhistos));
			}
		}
	}

	std::vector<TH1 *> mergedhistos;
	for(std::map<std::string, std::vector<TH1 *> >::iterator it = histos.begin(); it != histos.end(); ++it){
		mergedhistos.push_back(MergeBunch(it->second));
	}

	// Write to file
	// file name gets time stamp
	// ROOT file I/O probably not thread safe - needs locking
	if(fZMQconnector) fZMQconnector->SetLock();
	TFile *writer = new TFile(Form("HLTEMC_%09d_%d_%d_%d_%d_%d_%d.root", fRunNumber, timestamp.tm_year, timestamp.tm_mon, timestamp.tm_mday, timestamp.tm_hour, timestamp.tm_min, timestamp.tm_sec), "RECREATE");
	writer->cd();
	for(std::vector<TH1 *>::iterator histiter = mergedhistos.begin(); histiter != mergedhistos.end(); ++histiter){
		(*histiter)->Write();
	}
	writer->Close();
	delete writer;
	if(fZMQconnector) fZMQconnector->ReleaseLock();
	fWorking->Delete();
}

TH1 *Merger::MergeBunch(const std::vector<TH1 *> &content){
	TH1 *result(NULL);
	for(std::vector<TH1 *>::const_iterator it = content.begin(); it != content.end(); ++it){
		if(!result){
			// New content
			result = CopyHist(*it);
		} else {
			// existing content
			result->Add(*it);
		}
	}
	return result;
}

TH1 *Merger::CopyHist(const TH1 *input){
	// simple copy helper
	// Find correct type and call copy constructor
	TH1 *result(NULL);
	if(input->IsA() == TH1F::Class()){
		result = new TH1F(*static_cast<const TH1F *>(input));
	} else if(input->IsA() == TH1D::Class()){
		result = new TH1D(*static_cast<const TH1D *>(input));
	} else if(input->IsA() == TH1I::Class()){
		result = new TH1I(*static_cast<const TH1I *>(input));
	} else if(input->IsA() == TH1S::Class()){
		result = new TH1S(*static_cast<const TH1S *>(input));
	} else if(input->IsA() == TProfile::Class()){
		result = new TProfile(*static_cast<const TProfile *>(input));
	} else if(input->IsA() == TH2D::Class()){
		result = new TH2D(*static_cast<const TH2D *>(input));
	} else if(input->IsA() == TH2F::Class()){
		result = new TH2F(*static_cast<const TH2F *>(input));
	} else if(input->IsA() == TH2I::Class()){
		result = new TH2I(*static_cast<const TH2I *>(input));
	} else if(input->IsA() == TH2S::Class()){
		result = new TH2S(*static_cast<const TH2S *>(input));
	}
	return result;
}

void Merger::SetPayload(TObjArray *content){
	while(fInternalLock);
	fExternalLock = true;
	fPayloads->Add(content);
	fExternalLock = false;
}
