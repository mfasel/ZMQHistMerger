/*
 * Runnable.cpp
 *
 *  Created on: 21.11.2015
 *      Author: markusfasel
 */

#include "Runnable.h"

Runnable::Runnable() :
fthread()
{
}

bool Runnable::StartThread(){
	return pthread_create(&fthread, NULL, Runnable::ExecuteThread, this) == 0;
}

void Runnable::JoinThread(){
	pthread_join(fthread, NULL);
}

void *Runnable::ExecuteThread(void *content){
	reinterpret_cast<Runnable *>(content)->Run();
	return NULL;
}


