/*
 * main.cpp
 *
 *  Created on: 21.11.2015
 *      Author: markusfasel
 */

#include <map>
#include <string>

#include <TArrayI.h>
#include <TPRegExp.h>
#include <TRegExp.h>
#include <TString.h>

#include "Merger.h"
#include "ZMQconnector.h"

typedef std::map<std::string, std::string> stringMap;

TString GetFullArgString(int argc, char** argv);
stringMap* TokenizeOptionString(const TString str);
int ProcessOptionString(TString arguments);

TString fZMQconfigIN = "PULL@tcp://*60201:";
int fPollInterval = 0;
int fPollTimeout = 1000;
int fMergeInterval = 600;		// In seconds
bool fVerbose = kFALSE;

int main(int argc, const char **argv){

	Merger mymerger;
	ZMQconnector connector;

	// interconnect both threads
	mymerger.SetZMQConnector(&connector);
	connector.SetMerger(&mymerger);

	connector.SetConnectionString(fZMQconfigIN.Data());
	connector.SetPollingInterval(fPollInterval);
	connector.SetPollingTimeout(fPollTimeout);
	mymerger.SetMergingInterval(fMergeInterval);

	if(fVerbose){
		mymerger.SetVerbose(true);
		connector.SetVerbose(true);
	}

	// Start both threads
	connector.StartThread();
	mymerger.StartThread();

	// join threads
	mymerger.JoinThread();
	connector.JoinThread();
}

int ProcessOption(TString option, TString value) {
	//process option
	//to be implemented by the user

	//if (option.EqualTo("ZMQpollIn"))
	//{
	//  fZMQpollIn = (value.EqualTo("0"))?kFALSE:kTRUE;
	//}

	if (option.EqualTo("ZMQconfigIN") || option.EqualTo("in")) {
		fZMQconfigIN = value;
	}
	else if (option.EqualTo("Verbose")) {
		fVerbose=kTRUE;
	}
	else if (option.EqualTo("poll-timeout")) {
		fPollTimeout = value.Atoi();
	} else if(option.EqualTo("poll-interval")) {
		fPollInterval = value.Atoi();
	} else if(option.EqualTo("merge-interval")) {
		fMergeInterval = value.Atoi();
	}

	return 1;
}


TString GetFullArgString(int argc, char** argv){
	TString argString;
	TString argument="";
	if (argc>0) {
		for (int i=1; i<argc; i++) {
			argument=argv[i];
			if (argument.IsNull()) continue;
			if (!argString.IsNull()) argString+=" ";
			argString+=argument;
		}
	}
	return argString;
}

int ProcessOptionString(TString arguments){
	//process passed options
	stringMap* options = TokenizeOptionString(arguments);
	for (stringMap::iterator i=options->begin(); i!=options->end(); ++i)
	{
		//Printf("  %s : %s", i->first.data(), i->second.data());
		ProcessOption(i->first,i->second);
	}
	delete options; //tidy up

	return 1;
}


stringMap* TokenizeOptionString(const TString str) {
	//options have the form:
	// -option value
	// -option=value
	// -option
	// --option value
	// --option=value
	// --option
	// option=value
	// option value
	// (value can also be a string like 'some string')
	//
	// options can be separated by ' ' or ',' arbitrarily combined, e.g:
	//"-option option1=value1 --option2 value2, -option4=\'some string\'"

	//optionRE by construction contains a pure option name as 3rd submatch (without --,-, =)
	//valueRE does NOT match options
	TPRegexp optionRE("(?:(-{1,2})|((?='?[^,=]+=?)))"
			"((?(2)(?:(?(?=')'(?:[^'\\\\]++|\\.)*+'|[^, =]+))(?==?))"
			"(?(1)[^, =]+(?=[= ,$])))");
	TPRegexp valueRE("(?(?!(-{1,2}|[^, =]+=))"
			"(?(?=')'(?:[^'\\\\]++|\\.)*+'"
			"|[^, =]+))");

	stringMap* options = new stringMap;

	TArrayI pos;
	const TString mods="";
	Int_t start = 0;
	while (1) {
		Int_t prevStart=start;
		TString optionStr="";
		TString valueStr="";

		//check if we have a new option in this field
		Int_t nOption=optionRE.Match(str,mods,start,10,&pos);
		if (nOption>0) {
			optionStr = str(pos[6],pos[7]-pos[6]);
			optionStr=optionStr.Strip(TString::kBoth,'\'');
			start=pos[1]; //update the current character to the end of match
		}

		//check if the next field is a value
		Int_t nValue=valueRE.Match(str,mods,start,10,&pos);
		if (nValue>0) {
			valueStr = str(pos[0],pos[1]-pos[0]);
			valueStr=valueStr.Strip(TString::kBoth,'\'');
			start=pos[1]; //update the current character to the end of match
		}

		//skip empty entries
		if (nOption>0 || nValue>0) {
			(*options)[optionStr.Data()] = valueStr.Data();
		}

		if (start>=str.Length()-1 || start==prevStart ) break;
	}

	//for (stringMap::iterator i=options->begin(); i!=options->end(); ++i)
	//{
	//  printf("%s : %s\n", i->first.data(), i->second.data());
	//}
	return options;
}

